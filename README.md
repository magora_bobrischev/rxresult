*Wrapper for*

https://github.com/VictorAlbertos/RxActivityResult

*and*

https://github.com/tbruyelle/RxPermissions

Allows request permissions and show rationale dialog automagically when required.

