package app;

import android.app.Application;
import rx_activity_result.RxResult;

public class SampleApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        RxResult.register(this);
    }
}
