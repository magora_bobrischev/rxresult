package app;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import io.victoralbertos.app.R;
import rx_activity_result.RxResult;
import rx_activity_result.permissions_result.PermissionInfo;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
@SuppressWarnings("ConstantConditions")
public class ActivityPermissions extends AppCompatActivity {
    private static final String TAG = "M_PERMISSION";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions);

        findViewById(R.id.button_location).setOnClickListener(v -> requestLocationPermissions());

        findViewById(R.id.button_call_phone).setOnClickListener(v -> requestCallPhonePermissions());

        findViewById(R.id.button_camera).setOnClickListener(v -> requestCameraPermissions());

        findViewById(R.id.button_all).setOnClickListener(v -> requestAllPermissions());
    }

    private void requestLocationPermissions() {
        RxResult.checkPermissions(this,
                new PermissionInfo(Manifest.permission.ACCESS_FINE_LOCATION, "Location", "I Need Location Permissions", "Grant", "Cancel"))
                .subscribe(permission -> Log.i(TAG, permission.toString()));
    }

    private void requestCallPhonePermissions() {
        RxResult.checkPermissions(this,
                new PermissionInfo(Manifest.permission.CALL_PHONE, "Call Phone", "I want Your Phone to call", "Grant", "Cancel"))
                .subscribe(permission -> Log.i(TAG, permission.toString()));
    }

    private void requestCameraPermissions() {
        RxResult.checkPermissions(this,
                new PermissionInfo(Manifest.permission.CAMERA, "Camera", "Give. Me. The. Camera.", "Grant", "Cancel"))
                .subscribe(permission -> Log.i(TAG, permission.toString()));
    }

    private void requestAllPermissions() {
        PermissionInfo location = new PermissionInfo(
                Manifest.permission.ACCESS_FINE_LOCATION,
                "Location", "I need access to Your location data",
                "Approve", "Cancel");
        PermissionInfo callPhone = new PermissionInfo(
                Manifest.permission.CALL_PHONE,
                "Phone", "Give Me access to phone call. I want to call!!!!111",
                "Uh, OK", "GTFO");
        PermissionInfo camera = new PermissionInfo(
                Manifest.permission.CAMERA,
                "Camera", "I need a camera. Now.",
                "Here It is", "No Camera");
        RxResult.checkPermissions(this, location)
                .doOnNext(l -> Log.i(TAG, l.toString()))
                .flatMap(item -> RxResult.checkPermissions(this, callPhone))
                .doOnNext(l -> Log.i(TAG, l.toString()))
                .flatMap(item -> RxResult.checkPermissions(this, camera))
                .doOnNext(l -> Log.i(TAG, l.toString()))
                .subscribe();
    }
}
