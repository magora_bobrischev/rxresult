package app;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import io.victoralbertos.app.R;
import rx.Observable;
import rx_activity_result.RxResult;
import rx_activity_result.permissions_result.PermissionInfo;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class FragmentPermissions extends Fragment {
    private static final String TAG = "M_PERMISSION";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_permissions, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.button_location).setOnClickListener(v -> requestLocationPermissions());

        view.findViewById(R.id.button_call_phone).setOnClickListener(v -> requestCallPhonePermissions());

        view.findViewById(R.id.button_camera).setOnClickListener(v -> requestCameraPermissions());

        view.findViewById(R.id.button_all).setOnClickListener(v -> requestAllPermissions());
    }

    private void requestLocationPermissions() {
        RxResult.checkPermissions(getActivity(),
                new PermissionInfo(Manifest.permission.ACCESS_FINE_LOCATION, "Location", "I Need Location Permissions", "Grant", "Cancel"))
                .subscribe(permission -> Log.i(TAG, permission.toString()));
    }

    private void requestCallPhonePermissions() {
        RxResult.checkPermissions(getActivity(),
                new PermissionInfo(Manifest.permission.CALL_PHONE, "Call Phone", "I want Your Phone to call", "Grant", "Cancel"))
                .subscribe(permission -> Log.i(TAG, permission.toString()));
    }

    private void requestCameraPermissions() {
        RxResult.checkPermissions(getActivity(),
                new PermissionInfo(Manifest.permission.CAMERA, "Camera", "Give. Me. The. Camera.", "Grant", "Cancel"))
                .subscribe(permission -> Log.i(TAG, permission.toString()));
    }

    private void requestAllPermissions() {
        PermissionInfo location = new PermissionInfo(
                Manifest.permission.ACCESS_FINE_LOCATION,
                "Location", "I need access to Your location data",
                "Approve", "Cancel");
        PermissionInfo callPhone = new PermissionInfo(
                Manifest.permission.CALL_PHONE,
                "Phone", "Give Me access to phone call. I want to call!!!!111",
                "Uh, OK", "GTFO");
        PermissionInfo camera = new PermissionInfo(
                Manifest.permission.CAMERA,
                "Camera", "I need a camera. Now.",
                "Here It is", "No Camera");

        Observable.just(location, callPhone, camera)
                .flatMap(data -> RxResult.checkPermissions(getActivity(), data)).subscribe();
    }
}
