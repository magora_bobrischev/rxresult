package app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import io.victoralbertos.app.R;


@SuppressWarnings("ConstantConditions")
public class ActivityStart extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        findViewById(R.id.button_activity_for_result).setOnClickListener(view -> startActivity(new Intent(ActivityStart.this, ActivityForResult.class)));

        findViewById(R.id.buttont_fragment_for_result).setOnClickListener(view -> startActivity(new Intent(ActivityStart.this, ActivityWithFragmentForResult.class)));

        findViewById(R.id.button_permissions_activity).setOnClickListener(v -> startActivity(new Intent(ActivityStart.this, ActivityPermissions.class)));

        findViewById(R.id.button_permissions_fragment).setOnClickListener(v -> startActivity(new Intent(ActivityStart.this, ActivityWithFragmentsPermissions.class)));
    }
}
